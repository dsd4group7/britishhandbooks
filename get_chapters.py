import re

text = open("204486095.txt", encoding="utf8").read()
chapter_titles = ['THE\sLAND\sAND\sTHE\sPEOPLE', 'GOVERNMENT', 'LAW\sAND\sORDER']
num_of_chapters = 2
chapters = []

for i in range(num_of_chapters):
  text_after_chapter_start = re.split(chapter_titles[i], text, maxsplit=2, flags=re.MULTILINE)[2]
  chapter = re.split(chapter_titles[i+1], text_after_chapter_start, maxsplit=1, flags=re.MULTILINE)[0]
  chapters.append(chapter)

print(chapters)
