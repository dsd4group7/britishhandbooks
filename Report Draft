DS4D Assignment 3
Link to website:
Link to video:

Introduction & audience

Our dataset consists of 49 completely unexplored text files published as handbooks by the UK government between 1954-2005 for overseas reader to answer questions about Britain. The handbooks were provided by the National Library of Scotland.  The official handbooks are publication from the British government; thus, this is an interesting dataset as it could reflect the changes of the UK through time. Our audience is the school children, especial for those who consider history boring and would not take the time to look at the original handbooks. We aim to make improvements in school learning and stimulate children’s interest in history, help them to learn more about British history, in a more attractive and engaging way. 

Data processing

The data consists of pure text file and every document covers events happed in the UK over the previous year.  The length of each text file is more than one million characters. Therefore, we choose Spacy (a natural language processing method) to process the long text files. And our task is to clean up the long text files and focus on some specific aspects. 

When we use the Spacy to process the file, we found that the default length of processing file in Spacy is one million. Therefore, we have to set the maximum length manually (need more RAM) or split the file into several parts to process.

The spacy model will detect every signal word, punctuation, number as a token, and it could count their frequencies. Thus, we could only focus on words in the handbook. After counting the frequencies in every file, we transform the data into data frames and save them as CSV files. We could just analysis the CSV files without loading text files, which cost a lot of time. With processing all the files, we could find the variation trend in most common words with timeline, which could reflect the development or changes of the UK in the 50 years. 

Besides, we use name entity recognition in Spacy to make more accurate detection. Because token can only detect the signal word, for example, ‘Northern Ireland’, it should be a phrase, but the token detector will split them into two words. But with name entity recognition, it can be detected as one entity, and the label of it is ‘GPE’(Countries, states or cities). This is more reasonable for detecting a specific location or counties, whose name may be a phrase. 

Visualisation
To get a first impression of how the topics and the content-focus in the handbooks changed over time we used seaborn to create graphs visualizing the frequency-change of different word categories we discovered. The plotting of bar charts showing the length of each handbook and line charts visualizing the frequency of words associated with areas like ‘Scotland’ or ‘Wales’ helped us to identify which areas were of particular importance in certain times. The development of frequent used words like “industry”, “services”, “education” and “technology” provided an indication on how the government decided to present Britain and how the focus shifted from industry to education as well as technology becoming more important.  

We were able to connect some of these findings to important events in British history. For example, the frequency of the word “European” and “Europe” increased significantly in the year 1973 after the UK joint the European Communities and peaked in 1993 when the official European Union was founded. 

In terms of visualisation, we decide to build a website to show our analysis of the dataset. 

To increase the interest in the handbooks, especially among school children, and present the content in a more condensed and attractive way, we decided to create and interactive website allowing the user to engage with the content. Our website includes three main components: a timeline, a map and a quiz. On the timeline each handbook is represented by its cover picture. By hovering over the cover more information of each handbook gets displayed.  

The interactive map allows users to explore different regions in the UK and their role in the handbooks. The quiz will help the students to check how well they understood the content and what information they might have missed.  



Future work
Due to the extensive amount of data and limited time we were only able to conduct a very general content analysis. For future projects it might be interesting to select one of our findings and explore it in more detail. More data outside of the handbooks coudld be collected and linked to the findings. The outside data could be used to critically reflect how the government decided to present Britain in certain situations compared to representations of other sources. 

